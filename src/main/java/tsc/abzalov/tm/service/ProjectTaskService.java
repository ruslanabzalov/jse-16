package tsc.abzalov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.api.repository.IProjectRepository;
import tsc.abzalov.tm.api.repository.ITaskRepository;
import tsc.abzalov.tm.api.service.IProjectTaskService;
import tsc.abzalov.tm.exception.data.EmptyIdException;
import tsc.abzalov.tm.model.Project;
import tsc.abzalov.tm.model.Task;

import java.util.List;

import static org.apache.commons.lang3.StringUtils.isAnyBlank;
import static org.apache.commons.lang3.StringUtils.isBlank;

public final class ProjectTaskService implements IProjectTaskService {

    @NotNull
    private final IProjectRepository projectRepository;

    @NotNull
    private final ITaskRepository taskRepository;

    public ProjectTaskService(@NotNull final IProjectRepository projectRepository,
                              @NotNull final ITaskRepository taskRepository) {
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
    }

    @Override
    public int indexOf(@NotNull final Task task) {
        return taskRepository.indexOf(task);
    }

    @Override
    public boolean hasData() {
        return projectRepository.size() != 0 && taskRepository.size() != 0;
    }

    @Override
    public void addTaskToProjectById(@NotNull final String projectId, @NotNull final String taskId)  throws Exception {
        if (isAnyBlank(projectId, taskId)) throw new EmptyIdException();
        taskRepository.addTaskToProjectById(projectId, taskId);
    }

    @Override
    @Nullable
    public Project findProjectById(@NotNull final String id) throws Exception {
        if (isBlank(id)) throw new EmptyIdException();
        return projectRepository.findProjectById(id);
    }

    @Override
    @Nullable
    public Task findTaskById(@NotNull final String id) throws Exception {
        if (isBlank(id)) throw new EmptyIdException();
        return taskRepository.findTaskById(id);
    }

    @Override
    @Nullable
    public List<Task> findProjectTasksById(@NotNull final String projectId) throws Exception {
        if (isBlank(projectId)) throw new EmptyIdException();
        return taskRepository.findProjectTasksById(projectId);
    }

    @Override
    public void deleteProjectById(@NotNull final String id) throws Exception {
        if (isBlank(id)) throw new EmptyIdException();
        projectRepository.deleteProjectById(id);
    }

    @Override
    public void deleteProjectTasksById(@NotNull final String projectId) throws Exception {
        if (isBlank(projectId)) throw new EmptyIdException();
        taskRepository.deleteProjectTasksById(projectId);
    }

    @Override
    public void deleteProjectTaskById(@NotNull final String taskId) throws Exception {
        if (isBlank(taskId)) throw new EmptyIdException();
        taskRepository.deleteProjectTaskById(taskId);
    }

}
