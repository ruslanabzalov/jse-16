package tsc.abzalov.tm.service;

import org.jetbrains.annotations.NotNull;
import tsc.abzalov.tm.api.repository.ICommandRepository;
import tsc.abzalov.tm.api.service.ICommandService;
import tsc.abzalov.tm.model.Command;

public final class CommandService implements ICommandService {

    @NotNull
    private final ICommandRepository commandRepository;

    public CommandService(@NotNull final ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    @Override
    @NotNull
    public Command[] getCommands() {
        return commandRepository.getCommands();
    }

    @Override
    @NotNull
    public String[] getCommandNames() {
        return commandRepository.getCommandNames();
    }

    @Override
    @NotNull
    public String[] getCommandArgs() {
        return commandRepository.getCommandArgs();
    }

}
