package tsc.abzalov.tm;

import org.jetbrains.annotations.NotNull;
import tsc.abzalov.tm.bootstrap.CommandBootstrap;

public final class Application {

    public static void main(@NotNull String... args) {
        final CommandBootstrap commandBootstrap = new CommandBootstrap();
        commandBootstrap.run(args);
    }

}
