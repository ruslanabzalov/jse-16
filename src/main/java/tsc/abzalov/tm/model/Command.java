package tsc.abzalov.tm.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.model.enumeration.CommandType;

public final class Command {

    @NotNull
    private String name;

    @Nullable
    private String arg;

    @NotNull
    private String description;

    @NotNull
    private CommandType commandType;

    public Command(@NotNull final String name, @Nullable final String arg, @NotNull final String description,
                   @NotNull final CommandType commandType) {
        this.name = name;
        this.arg = arg;
        this.description = description;
        this.commandType = commandType;
    }

    @NotNull
    public String getName() {
        return this.name;
    }

    public void setName(@NotNull final String name) {
        this.name = name;
    }

    @Nullable
    public String getArg() {
        return this.arg;
    }

    public void setArg(@Nullable final String arg) {
        this.arg = arg;
    }

    @NotNull
    public String getDescription() {
        return this.description;
    }

    public void setDescription(@NotNull final String description) {
        this.description = description;
    }

    @NotNull
    public CommandType getCommandType() {
        return commandType;
    }

    public void setCommandType(@NotNull final CommandType commandType) {
        this.commandType = commandType;
    }

    @Override
    public String toString() {
        final String correctArg = (this.arg == null || this.arg.isEmpty()) ? ": " : " [" + this.arg + "]: ";
        return this.name + correctArg + this.description;
    }

}
