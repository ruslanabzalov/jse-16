package tsc.abzalov.tm.api.repository;

import tsc.abzalov.tm.model.Project;

import java.util.List;

public interface IProjectRepository {

    int size();

    int indexOf(Project project);

    void createProject(String id, String description);

    List<Project> findAllProjects();

    Project findProjectById(String id);

    Project findProjectByIndex(int index);

    Project findProjectByName(String name);

    Project updateProjectById(String id, String name, String description);

    Project updateProjectByIndex(int index, String name, String description);

    Project updateProjectByName(String name, String description);

    void deleteAllProjects();

    void deleteProjectById(String id);

    void deleteProjectByIndex(int index);

    void deleteProjectByName(String name);

    Project startProjectById(String id);

    Project endProjectById(String id);

}
