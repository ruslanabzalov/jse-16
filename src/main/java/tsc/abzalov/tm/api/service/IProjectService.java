package tsc.abzalov.tm.api.service;

import tsc.abzalov.tm.model.Project;

import java.util.List;

public interface IProjectService {

    int size();

    int indexOf(Project project);

    void createProject(String name, String description) throws Exception;

    List<Project> getAllProjects();

    Project getProjectById(String id) throws Exception;

    Project getProjectByIndex(int index) throws Exception;

    Project getProjectByName(String name) throws Exception;

    Project updateProjectById(String id, String name, String description) throws Exception;

    Project updateProjectByIndex(int index, String name, String description) throws Exception;

    Project updateProjectByName(String name, String description) throws Exception;

    void removeAllProjects();

    void removeProjectById(String id) throws Exception;

    void removeProjectByIndex(int index) throws Exception;

    void removeProjectByName(String name) throws Exception;

    Project startProjectById(String id) throws Exception;

    Project endProjectById(String id) throws Exception;

    List<Project> sortProjectsByName();

    List<Project> sortProjectsByStartDate();

    List<Project> sortProjectsByEndDate();

    List<Project> sortProjectsByStatus();
    
}
