package tsc.abzalov.tm.api.service;

import tsc.abzalov.tm.model.Task;

import java.util.List;

public interface ITaskService {

    int size();

    int indexOf(Task task);

    void createTask(String name, String description) throws Exception;

    List<Task> getAllTasks();

    Task getTaskById(String id) throws Exception;

    Task getTaskByIndex(int index) throws Exception;

    Task getTaskByName(String name) throws Exception;

    Task updateTaskById(String id, String name, String description) throws Exception;

    Task updateTaskByIndex(int index, String name, String description) throws Exception;

    Task updateTaskByName(String name, String description) throws Exception;

    void removeAllTasks();

    void removeTaskById(String id) throws Exception;

    void removeTaskByIndex(int index) throws Exception;

    void removeTaskByName(String name) throws Exception;

    Task startTaskById(String id) throws Exception;

    Task endTaskById(String id) throws Exception;

    List<Task> sortTasksByName();

    List<Task> sortTasksByStartDate();

    List<Task> sortTasksByEndDate();

    List<Task> sortTasksByStatus();
    
}
