package tsc.abzalov.tm.api.controller;

public interface ITaskController {

    void createTask() throws Exception;

    void showAllTasks();

    void showTaskById() throws Exception;

    void showTaskByIndex() throws Exception;

    void showTaskByName() throws Exception;

    void editTaskById() throws Exception;

    void editTaskByIndex() throws Exception;

    void editTaskByName() throws Exception;

    void deleteAllTasks();

    void deleteTaskById() throws Exception;

    void deleteTaskByIndex() throws Exception;

    void deleteTaskByName() throws Exception;

    void startTaskById() throws Exception;

    void endTaskById() throws Exception;

    void showTasksSortedByName();

    void showTasksSortedByStartDate();

    void showTasksSortedByEndDate();

    void showTasksSortedByStatus();

}
