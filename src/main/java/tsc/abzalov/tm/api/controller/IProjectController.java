package tsc.abzalov.tm.api.controller;

public interface IProjectController {

    void createProject() throws Exception;

    void showAllProjects();

    void showProjectById() throws Exception;

    void showProjectByIndex() throws Exception;

    void showProjectByName() throws Exception;

    void editProjectById() throws Exception;

    void editProjectByIndex() throws Exception;

    void editProjectByName() throws Exception;

    void deleteAllProjects();

    void deleteProjectById() throws Exception;

    void deleteProjectByIndex() throws Exception;

    void deleteProjectByName() throws Exception;

    void startProjectById() throws Exception;

    void endProjectById() throws Exception;

    void showProjectsSortedByName();

    void showProjectsSortedByStartDate();

    void showProjectsSortedByEndDate();

    void showProjectsSortedByStatus();

}
