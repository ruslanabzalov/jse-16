package tsc.abzalov.tm.constant;

public final class LiteralConst {

    public static final String DEFAULT_DESCRIPTION = "Description is empty";

    public static final String DEFAULT_REFERENCE = "Empty";

    public static final String IS_NOT_STARTED = "Is not started";

    public static final String IS_NOT_ENDED = "Is not ended";

    public static final String DATE_TIME_FORMAT = "dd.MM.yyyy HH:mm:ss";

    private LiteralConst() {
    }

}
